import org.junit.Assert;
import org.junit.Test;

public class GameIntegrationTest {

    @Test
    public void shouldPlayGameBetweenAlwaysCheatAndAlwaysCooperate() {

        Player alwaysCheat = new Player("p1", PlayerBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        Player alwaysCooperate = new Player("p1", PlayerBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        Machine machine=new Machine();
        Game game = new Game(alwaysCheat,alwaysCooperate,machine,5);
        game.play();
        Assert.assertEquals("Player1 score is : 15 and Player2 score is -5",game.displayScore());
    }


    @Test
    public void shouldPlayGameBetweenCopyCatAndAlwaysCheat() {
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour("p2");
        Player copyCatPlayer = new Player("p1",copyCatBehaviour);
        Player alwaysCheatPlayer = new Player("p2",PlayerBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        Machine machine = new Machine();
        Game game = new Game(copyCatPlayer,alwaysCheatPlayer,machine,5);
        game.addObserver(copyCatBehaviour);
        game.play();
        Assert.assertEquals("Player1 score is : -1 and Player2 score is 3",game.displayScore());

    }

    @Test
    public void shouldPlayGameBetweenCopyCatAndDetective() {

        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour("Detective");
        Player copyCatPlayer = new Player("CopyCat",copyCatBehaviour);
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour("CopyCat");
        Player detectivePlayer = new Player("Detective",detectiveBehaviour);
        Machine machine = new Machine();
        Game game = new Game(copyCatPlayer,detectivePlayer,machine,5);
        game.addObserver(copyCatBehaviour);
        game.addObserver(detectiveBehaviour);
        game.play();
        Assert.assertEquals("Player1 score is : 8 and Player2 score is 8",game.displayScore());
    }
}
