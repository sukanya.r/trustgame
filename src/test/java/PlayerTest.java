import org.junit.Assert;
import org.junit.Test;

public class PlayerTest {

    @Test
    public void updateScore() {
        Player player = new Player("p1", ()->null);
        player.updateScore(10);
        Assert.assertEquals(10,player.getScore());
    }
}