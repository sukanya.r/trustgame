import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class CopyCatBehaviourTest {

    @Test
    public void shouldReturnCooperateForFirstMove(){
        Player copyCatPlayer = new Player("p1", new CopyCatBehaviour(""));
        Assert.assertEquals(Move.Cooperate,copyCatPlayer.makeMove());
    }
    @Test
    public void shouldReturnOpponentsMoveAfterFirstMove(){

        String opponentName = "opponent1";
        CopyCatBehaviour copyCatBehaviour = new CopyCatBehaviour(opponentName);
        HashMap<String, Move> map = new HashMap<>();
        map.put(opponentName, Move.Cheat);
        copyCatBehaviour.update(null,map);
        Player copyCatPlayer = new Player("p1", copyCatBehaviour);
        Assert.assertEquals(Move.Cheat,copyCatPlayer.makeMove());
    }
}
