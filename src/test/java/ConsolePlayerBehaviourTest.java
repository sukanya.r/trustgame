import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class ConsolePlayerBehaviourTest {

    @Test
    public void shouldReturnPlayersMoveAsCooperateIfPlayerEntersOne() {

        Player player = new Player("p1", new ConsolePlayerBehaviour(new Scanner("1")));
        Move move = player.makeMove();
        Assert.assertEquals(Move.Cooperate,move);
    }

    @Test
    public void shouldReturnPlayersMoveAsCheatIfPlayerEntersTwo() {
        Player player = new Player("p1", new ConsolePlayerBehaviour(new Scanner("2")));
        Move move = player.makeMove();
        Assert.assertEquals(Move.Cheat,move);
    }

    @Test
    public void shouldAllowPlayerToEnterValidInputIfPlayerEntersInvalidNumericInput() {
        Player player = new Player("p1", new ConsolePlayerBehaviour(new Scanner("3\n4\n2")));
        Move move = player.makeMove();
        Assert.assertEquals(Move.Cheat,move);
    }

    @Test
    public void shouldAllowPlayerToEnterValidInputIfPlayerEntersInvalidNonNumericInput() {
        Player player = new Player("p1", new ConsolePlayerBehaviour(new Scanner("random\n2")));
        Move move = player.makeMove();
        Assert.assertEquals(Move.Cheat,move);
    }



}
