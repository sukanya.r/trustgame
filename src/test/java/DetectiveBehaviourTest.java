import org.junit.Test;

import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;

public class DetectiveBehaviourTest {

    private String opponentName="Opponent";

    @Test
    public void shouldReturnCooperateCheatCooperateCooperate() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(opponentName);
        Player player = new Player("Detective", detectiveBehaviour);

        Move firstExpectedMove = Move.Cooperate;
        Move secondExpectedMove = Move.Cheat;
        Move thirdExpectedMove = Move.Cooperate;
        Move fourthExpectedMove = Move.Cooperate;

        HashMap<String, Move> map = new HashMap<>();
        map.put(opponentName, Move.Cheat);
        detectiveBehaviour.update(null, map);
        Move firstActualMove = player.makeMove();
        assertEquals(firstExpectedMove, firstActualMove);

        map.put(opponentName, Move.Cooperate);
        detectiveBehaviour.update(null, map);
        Move secondActualMove = player.makeMove();
        assertEquals(secondExpectedMove, secondActualMove);

        map.put(opponentName, Move.Cooperate);
        detectiveBehaviour.update(null, map);
        Move thirdActualMove = player.makeMove();
        assertEquals(thirdExpectedMove, thirdActualMove);

        map.put(opponentName, Move.Cheat);
        detectiveBehaviour.update(null, map);
        Move fourthActualMove = player.makeMove();
        assertEquals(fourthExpectedMove, fourthActualMove);

    }


    @Test
    public void shouldReturnCheatIfOpponentsCooperateAfterAnalyzing() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(opponentName);
        HashMap<String,Move> map = new HashMap<>();
        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        assertEquals(Move.Cheat,detectiveBehaviour.makeMove());
    }

    @Test
    public void shouldShowCopyCatBehaviourIfOpponentsCheats() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(opponentName);
        HashMap<String,Move> map = new HashMap<>();
        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent", Move.Cheat);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent", Move.Cheat);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent", Move.Cooperate);
        detectiveBehaviour.update(null, map);
        Move actualMove = detectiveBehaviour.makeMove();
        assertEquals(Move.Cooperate, actualMove);
    }

    @Test
    public void shouldConfirmCopyCatBehaviourForSixthMove() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(opponentName);
        HashMap<String,Move> map = new HashMap<>();
        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent", Move.Cheat);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent", Move.Cheat);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();
        map.put("Opponent", Move.Cooperate);
        detectiveBehaviour.makeMove();
        map.put("Opponent", Move.Cheat);
        Move actualMove = detectiveBehaviour.makeMove();
        assertEquals(Move.Cheat, actualMove);
    }

    @Test
    public void shouldReturnCheatForFifthStepIfCheatedOnLastStep() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(opponentName);
        HashMap<String,Move> map = new HashMap<>();
        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();

        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();

        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();

        map.put("Opponent",Move.Cheat);
        detectiveBehaviour.update(null,map);
        detectiveBehaviour.makeMove();

        map.put("Opponent",Move.Cooperate);
        detectiveBehaviour.update(null,map);
        Move actualMove = detectiveBehaviour.makeMove();
        assertEquals(Move.Cooperate, actualMove);
    }
}
