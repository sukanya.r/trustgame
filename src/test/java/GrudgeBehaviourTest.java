import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;

public class GrudgeBehaviourTest {

    @Test
    public void shouldAlwaysCooperate() {

        Player grudgePlayer = new Player("grudge", new GrudgeBehaviour("p1"));
        Assert.assertEquals(Move.Cooperate, grudgePlayer.makeMove());

    }

    @Test
    public void shouldCheatIfOpponentStartsCheating() {
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour("opponent");
        Player grudgePlayer = new Player("grudge", grudgeBehaviour);
        Player opponentPlayer = new Player("opponent", PlayerBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        HashMap<String, Move> map = new HashMap<>();
        map.put(opponentPlayer.getName(), opponentPlayer.makeMove());
        grudgeBehaviour.update(null, map);
        Assert.assertEquals(Move.Cheat, grudgePlayer.makeMove());
    }

    @Test
    public void shouldCooperateIfOpponentCooperates()
    {
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour("opponent");
        Player grudgePlayer = new Player("grudge", grudgeBehaviour);
        Player opponentPlayer = new Player("opponent", PlayerBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        HashMap<String, Move> map = new HashMap<>();
        map.put(opponentPlayer.getName(), opponentPlayer.makeMove());
        grudgeBehaviour.update(null, map);
        Assert.assertEquals(Move.Cooperate, grudgePlayer.makeMove());
    }

    @Test
    public void shouldCheatIfOpponentCheatAndThenCooperate()
    {
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour("opponent");
        Player grudgePlayer = new Player("grudge", grudgeBehaviour);
        HashMap<String, Move> map = new HashMap<>();
        Player opponentPlayer = new Player("opponent", PlayerBehaviour.ALWAYS_CHEAT_BEHAVIOUR);
        map.put(opponentPlayer.getName(), opponentPlayer.makeMove());
        grudgeBehaviour.update(null, map);
        Assert.assertEquals(Move.Cheat, grudgePlayer.makeMove());
        opponentPlayer = new Player("opponent", PlayerBehaviour.ALWAYS_COOPERATE_BEHAVIOUR);
        map.put(opponentPlayer.getName(), opponentPlayer.makeMove());
        grudgeBehaviour.update(null, map);
        Assert.assertEquals(Move.Cheat, grudgePlayer.makeMove());
    }
}
