import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

public class GameTest {

    private Player player1;
    private Player player2;
    private Machine machine;
    private Move player1Move;
    private Move player2Move;
    private int player1Score;
    private int player2Score;

    @Before
    public void setup() {

        player1 = mock(Player.class);
        player2 = mock(Player.class);
        machine = mock(Machine.class);
        player1Move = Move.Cheat;
        player2Move = Move.Cooperate;
        when(player1.makeMove()).thenReturn(player1Move);
        when(player2.makeMove()).thenReturn(player2Move);
        player1Score = 88;
        player2Score = 12;
        when(machine.calculateScore(player1Move, player2Move)).thenReturn(new int[]{player1Score, player2Score});

    }

    @Test
    public void shouldInvokePlayersMakeMoveMethodAndMachineCalculateScoreMethod() {


        new Game(player1, player2, machine, 1).play();
        verify(player1, times(1)).makeMove();
        verify(player2, times(1)).makeMove();
        verify(machine, times(1)).calculateScore(player1Move, player2Move);
    }

    @Test
    public void shouldPlayFiveRounds() {


        new Game(player1, player2, machine, 5).play();
        verify(player1, times(5)).makeMove();
        verify(player2, times(5)).makeMove();
        verify(machine, times(5)).calculateScore(player1Move, player2Move);
    }

    @Test
    public void playerShouldHaveScoreWhenGameFinishes() {

        Integer player1CurrentScore = 44;
        Integer player2CurrentScore = 66;
        when(player1.getScore()).thenReturn(player1CurrentScore);
        when(player2.getScore()).thenReturn(player2CurrentScore);

        new Game(player1, player2, machine, 1).play();

        verify(player1).updateScore(player1CurrentScore + player1Score);
        verify(player2).updateScore(player2CurrentScore + player2Score);
    }

//    @Test
//    public void shouldReturnScoreIfPlayMethodIsNotCalled() {
//
//        Game game = new Game(player1, player2, machine, 2);
//        String message = game.displayScore();
//        when(player1.getScore()).thenReturn(0);
//        when(player2.getScore()).thenReturn(0);
//        Assert.assertEquals("Player1 Score is : 0 and Player2 Score is 0",message);
//    }

    @Test
    public void shouldReturnScoreIfPlayMethodIsNotCalled() {

        Game game = new Game(player1, player2, machine, 2);
        String message = game.displayScore();
        when(player1.getScore()).thenReturn(0);
        when(player2.getScore()).thenReturn(0);
        Assert.assertEquals("Player1 score is : 0 and Player2 score is 0",message);
    }
}

