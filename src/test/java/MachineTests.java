import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MachineTests {

    private Machine machine;

    @Before
    public void setUp() {
        machine = new Machine();
    }

    @Test
    public void shouldCalculateCorrectScoreWhenBothPlayersCooperate() {
        int[] actualScores = machine.calculateScore(Move.Cooperate, Move.Cooperate);

        Assert.assertEquals(2, actualScores[0]);
        Assert.assertEquals(2, actualScores[1]);
    }

    @Test
    public void shouldCalculateCorrectScoreWhenPlayer1CheatsAndPlayer2Cooperates() {
        int[] actualScores = machine.calculateScore(Move.Cheat, Move.Cooperate);

        Assert.assertEquals(3, actualScores[0]);
        Assert.assertEquals(-1, actualScores[1]);
    }

    @Test
    public void shouldCalculateCorrectScoreWhenPlayer1CooperatesAndPlayer2Cheats() {
        int[] actualScores = machine.calculateScore(Move.Cooperate, Move.Cheat);

        Assert.assertEquals(-1, actualScores[0]);
        Assert.assertEquals(3, actualScores[1]);
    }

    @Test
    public void shouldCalculateCorrectScoreWhenPlayer1AndPlayer2Cheats() {
        int[] actualScores = machine.calculateScore(Move.Cheat, Move.Cheat);

        Assert.assertEquals(0, actualScores[0]);
        Assert.assertEquals(0, actualScores[1]);
    }


}
