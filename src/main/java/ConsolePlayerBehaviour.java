import java.util.Scanner;

public class ConsolePlayerBehaviour implements PlayerBehaviour {
    private Scanner scanner;

    public ConsolePlayerBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public Move makeMove() {
        int input = 0;
        do {
            try {
                input = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
            }

        }
        while (input != 1 && input != 2);
        if (input == 2)
            return Move.Cheat;
        return Move.Cooperate;
    }
}