import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class CopyCatBehaviour implements PlayerBehaviour, Observer {
    private Move opponentMove = Move.Cooperate;
    private String opponentName;

    public CopyCatBehaviour(String opponentName) {

        this.opponentName = opponentName;
    }

    @Override
    public Move makeMove() {
        return opponentMove;
    }

    @Override
    public void update(Observable o, Object arg) {

        HashMap<String, Move> moves = (HashMap<String, Move>) arg;
        opponentMove = moves.get(opponentName);
    }
}
