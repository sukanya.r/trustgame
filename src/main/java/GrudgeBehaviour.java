import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

public class GrudgeBehaviour implements PlayerBehaviour, Observer {

    private final String opponentName;
    private Move opponentMove=Move.Cooperate;
    private boolean cheatedOnce = false;

    public GrudgeBehaviour(String opponentName) {
        this.opponentName = opponentName;
    }

    @Override
    public Move makeMove() {
        if(cheatedOnce) return Move.Cheat;
        return opponentMove;
    }

    public void update(Observable o, Object arg) {
        HashMap<String, Move> map = (HashMap<String, Move>) arg;
        opponentMove = map.get(opponentName);
        if(opponentMove == Move.Cheat) cheatedOnce = true;
    }
}
