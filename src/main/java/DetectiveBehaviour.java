import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;
import java.util.Stack;

public class DetectiveBehaviour extends Observable implements PlayerBehaviour, Observer {

    private Stack<Move> moveStack;
    private final String opponentName;
    private Move opponentMove;
    private PlayerBehaviour playerBehaviour;

    public DetectiveBehaviour(String opponentName) {
        this.opponentName = opponentName;
        initStack();
    }

    private void initStack() {
        moveStack = new Stack<>();
        moveStack.push(Move.Cooperate);
        moveStack.push(Move.Cooperate);
        moveStack.push(Move.Cheat);
        moveStack.push(Move.Cooperate);
    }

    @Override
    public Move makeMove() {
        if (moveStack.empty()) {
            return playerBehaviour.makeMove();
        }
        return moveStack.pop();
    }


    @Override
    public void update(Observable o, Object arg) {
        HashMap<String,Move> map = (HashMap<String, Move>) arg;
        opponentMove = map.get(opponentName);
        setChanged();
        notifyObservers(arg);
        if (playerBehaviour == null) {
            if (opponentMove == Move.Cheat) {
                playerBehaviour = new CopyCatBehaviour(opponentName);
                addObserver((Observer) playerBehaviour);
            } else if (moveStack.empty()) {
                playerBehaviour = PlayerBehaviour.ALWAYS_CHEAT_BEHAVIOUR;
            }
        }
    }
}
