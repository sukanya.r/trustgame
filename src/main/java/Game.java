import java.util.HashMap;
import java.util.Observable;

public class Game extends Observable {

    private final int noOfRounds;
    private Player player1;
    private Player player2;
    private Machine machine;
    private HashMap<String, Move> map;

    public Game(Player player1, Player player2, Machine machine, int noOfRounds) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.noOfRounds = noOfRounds;
    }

    public void play() {

        for(int i = 0;i<noOfRounds;i++) {
            Move player1Move = player1.makeMove();
            Move player2Move = player2.makeMove();
            int[] scores = machine.calculateScore(player1Move, player2Move);
            map = new HashMap<>();
            map.put(player1.getName(),player1Move);
            map.put(player2.getName(),player2Move);
            setChanged();
            notifyObservers(map);
            player1.updateScore(scores[0]+player1.getScore());
            player2.updateScore(scores[1]+player2.getScore());
        }


    }

    public String displayScore() {

        return "Player1 score is : "+player1.getScore()+" and Player2 score is "+player2.getScore();
    }
}
