public class Machine {

    private static final int WIN_SCORE = 3;
    private static final int LOSE_SCORE = -1;
    private static final int COOPERATE_SCORE = 2;
    private static final int CHEAT_SCORE = 0;

    public int[] calculateScore(Move player1Move, Move player2Move) {

        if(player1Move == Move.Cooperate) {
            if (player2Move == Move.Cooperate)
                return new int[]{COOPERATE_SCORE, COOPERATE_SCORE};

            return new int[]{LOSE_SCORE, WIN_SCORE};
        }
        else
        {
            if (player2Move == Move.Cooperate)
                return new int[]{WIN_SCORE, LOSE_SCORE};

            return new int[]{CHEAT_SCORE, CHEAT_SCORE};
        }
    }
}
