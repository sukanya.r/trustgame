public class Player {

    private final PlayerBehaviour playerBehaviour;
    private final String name;
    private int score;

    public Player(String name, PlayerBehaviour playerBehaviour) {
        this.playerBehaviour = playerBehaviour;
        this.name = name;
    }

    public Move makeMove() {
        return playerBehaviour.makeMove();
    }

    public int getScore() {
        return score;
    }

    public void updateScore(int score) {
        this.score = score;
    }

    public String getName() {
        return this.name;
    }
}
