public interface OpponentMoveObserver {
    void setOpponentMove(Move move);
}
