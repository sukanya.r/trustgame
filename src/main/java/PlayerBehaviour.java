public interface PlayerBehaviour {
    PlayerBehaviour ALWAYS_CHEAT_BEHAVIOUR = () -> Move.Cheat;
    PlayerBehaviour ALWAYS_COOPERATE_BEHAVIOUR = () -> Move.Cooperate;

    Move makeMove();
}
